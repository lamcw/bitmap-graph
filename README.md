# Bitmap Graph
A library written in C that uses a bitmap to represent adjacency matrix.

## Usage
Check out test/test.c for sample usage. Detailed documentation in wiki.

## LICENSE
[MIT](LICENSE.md)
